from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
#from django_countries.fields import CountryField
from cities_light.models import Country, City, Region
from django.contrib import admin

class SurveyResponse(models.Model):

    ENGINEERING_MAJORS = ['Ingeniería Aeronáutica', 'Ingeniería Ambiental', 'Ingeniería Biomédica', 'Ingeniería Civil',
                          'Ingeniería Electromecánica', 'Ingeniería Electrónica', 'Ingeniería Industrial',
                          'Ingeniería Mecánica', 'Ingeniería Química',
                          'Ingeniería en Agrimensura', 'Ingeniería en Computación']
    ENGINEERING_MAJORS_CHOICES = [(f'{id}', major) for id, major in enumerate(ENGINEERING_MAJORS)]



    #on_delete cascade -> if a user is deleted then the agent linked to that user will be erased also
    #user has username,password,email,firstname and lastname attributes. That ones must not be redefined at Agente.
    mail = models.EmailField(unique=True, verbose_name='Correo Electronico')
    nombre = models.CharField(max_length=100, null=False, default='', verbose_name='Nombre')
    apellido = models.CharField(max_length=100, null=False, default='', verbose_name='Apellido')
    country = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True, verbose_name='País')
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True, verbose_name='Provincia/Region')
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, verbose_name='Ciudad')
    remoto = models.CharField(choices=[('1', 'Si'), ('0', 'No')], verbose_name='remoto', null=False, max_length=1)
    major = models.CharField(choices=ENGINEERING_MAJORS_CHOICES, verbose_name='Carrera', null=False, max_length=2)
    question_expectancy_text_field = models.TextField(max_length=1500, blank=False, verbose_name='expectancy')
    created_at = models.DateTimeField(auto_now_add=True)


#admin.site.register(SurveyResponse)