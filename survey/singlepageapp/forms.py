from django import forms
from .models import SurveyResponse
from cities_light.models import Country, City, Region

EMPTY = "--------"

class SurveyResponseForm(forms.ModelForm):
    class Meta:
        model = SurveyResponse
        #fields = ('nombre', 'apellido', 'cuit', 'localidad', 'calle', 'numero', 'telefono', 'mail', 'descripcion')
        exclude = ()
        labels = {'country': 'País (Desde dónde cursaran la materia)',
                  'region':'Provincia/Region (Desde dónde cursará la materia)',
                  'city':'Ciudad (Desde dónde cursará la materia)',
                  'remoto':'¿El lugar desde dónde cursará la materia coincide con su lugar de origen?',
                  'question_expectancy_text_field':'¿Que espera aprender en el curso?'}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['region'].queryset = City.objects.none()
        self.fields['city'].queryset = City.objects.none()

        if 'country' in self.data:
            try:
                country_id = int(self.data.get('country'))
                self.fields['region'].queryset = Region.objects.filter(country_id=country_id).order_by('name')
                if 'region' in self.data:
                    region_id = int(self.data.get('region'))
                    self.fields['city'].queryset = City.objects.filter(region_id=region_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['region'].queryset = self.instance.country.region_set.order_by('name')
            self.fields['city'].queryset = self.instance.country.city_set.order_by('name')
