from django.contrib import admin
from .models import SurveyResponse

# Register your models here.
@admin.register(SurveyResponse)
class QualificationAdmin(admin.ModelAdmin):
    list_display = [field.name for field in SurveyResponse._meta.fields]
    list_display.insert(0, '__str__')