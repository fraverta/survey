from django.conf.urls import url
from . import views
from django.urls import path


urlpatterns = [
    url(r'^surveyentry/new/$', views.survey_entry_new, name='survey_entry_new'),
    path('ajax/load-regions/', views.load_region, name='ajax_load_regions'),
    path('ajax/load-cities/', views.load_cities, name='ajax_load_cities'),
    url(r'^surveyentry/map/$', views.display_map, name='display_map'),
    url(r'^surveyentry/majors/$', views.display_pie_plot, name='display_map'),

]
