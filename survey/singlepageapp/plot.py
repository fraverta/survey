import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from cities_light.models import Country, City, Region
from .models import SurveyResponse
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
from collections import Counter

def update_students_location_map():
    print("hello")
    qs = SurveyResponse.objects.all()
    latitudes = []
    longitudes = []
    for row in qs:
        latitudes.append(float(row.city.latitude))
        longitudes.append(float(row.city.longitude))
        #cities_name.append(str(city))

    margin = 10 # buffer to add to the range
    lat_min = min(latitudes) - margin
    lat_max = max(latitudes) + margin
    lon_min = min(longitudes) - margin
    lon_max = max(longitudes) + margin
    # create map using BASEMAP
    m = Basemap(llcrnrlon=lon_min,
                llcrnrlat=lat_min,
                urcrnrlon=lon_max,
                urcrnrlat=lat_max,
                lat_0=(lat_max - lat_min)/2,
                lon_0=(lon_max-lon_min)/2,
                projection='merc',
                resolution = 'h',
                area_thresh=10000.,
                )
    m.drawcoastlines()
    m.drawcountries()
    m.drawstates()
    #m.drawmapboundary(fill_color='#46bcec')
    #m.fillcontinents(color = 'white',lake_color='#46bcec')
    m.shadedrelief()
    # convert lat and lon to map projection coordinates
    lons, lats = m(longitudes, latitudes)
    # plot points as red dots
    m.scatter(lons, lats, marker='X', color='r', s=12)
    plt.savefig('singlepageapp/static/singlepageapp/map-with-points.png', bbox_inches='tight', transparent=True, dpi=500)
    print("save")


def update_students_major_graph():
    c = Counter(SurveyResponse.objects.values_list('major', flat=True))
    data = [c[k] for k in sorted(c.keys())]
    majors = [f'{c[k]} - {SurveyResponse.ENGINEERING_MAJORS[int(k)]}' for k in sorted(c.keys())]
    print(majors)
    print(data)

    fig, ax = plt.subplots(figsize=(18, 9), subplot_kw=dict(aspect="equal"))

    wedges, texts = ax.pie(data, wedgeprops=dict(width=0.5), startangle=-40, textprops={'color': "w"})
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    kw = dict(arrowprops=dict(arrowstyle="-"),
              bbox=bbox_props, zorder=0, va="center", fontsize=20)

    for i, p in enumerate(wedges):
        ang = (p.theta2 - p.theta1) / 2. + p.theta1
        y = np.sin(np.deg2rad(ang))
        x = np.cos(np.deg2rad(ang))
        horizontalalignment = {-1: "right", 1: "left"}[int(np.sign(x))]
        connectionstyle = "angle,angleA=0,angleB={}".format(ang)
        kw["arrowprops"].update({"connectionstyle": connectionstyle})
        ax.annotate(majors[i], xy=(x, y), xytext=(1.35 * np.sign(x), 1.4 * y),
                    horizontalalignment=horizontalalignment, **kw)

    # ax.set_title("")
    plt.savefig('singlepageapp/static/singlepageapp/pie_plot.png', bbox_inches='tight', transparent=True, dpi=500)
    #plt.show()

'''
df = pd.read_csv('surveyresponse.csv')


latitudes = []
longitudes = []
cities_name = []
for index, row in df.iterrows():
    #print(row['city'])
    city = City.objects.get(pk=int(row['city']))
    latitudes.append(float(city.latitude))
    longitudes.append(float(city.longitude))
    cities_name.append(str(city))


df.insert(3,'latitude', latitudes)
df.insert(4,'longitude', longitudes)
df.insert(2,'city_name', cities_name)
df.to_csv('surveyresponse2.csv')


df = pd.read_csv('surveyresponse2.csv')
print("Longitud X")
print(round(df.longitude.min(),2))
print(round(df.longitude.max(),2))
print("Latitud Y")
print(round(df.latitude.min(),2))
print(round(df.latitude.max(),2))

margin = 10 # buffer to add to the range
lat_min = df.latitude.min() - margin
lat_max = df.latitude.max() + margin
lon_min = df.longitude.min() - margin
lon_max = df.longitude.max() + margin
# create map using BASEMAP
m = Basemap(llcrnrlon=lon_min,
            llcrnrlat=lat_min,
            urcrnrlon=lon_max,
            urcrnrlat=lat_max,
            lat_0=(lat_max - lat_min)/2,
            lon_0=(lon_max-lon_min)/2,
            projection='merc',
            resolution = 'h',
            area_thresh=10000.,
            )
m.drawcoastlines()
m.drawcountries()
m.drawstates()
#m.drawmapboundary(fill_color='#46bcec')
#m.fillcontinents(color = 'white',lake_color='#46bcec')
m.shadedrelief()
# convert lat and lon to map projection coordinates
lat = df['latitude'].values
lon = df['longitude'].values
lons, lats = m(lon, lat)
# plot points as red dots
m.scatter(lons, lats, marker = 'X', color='r', s=12)
plt.savefig('/home/fraverta/Dropbox/Informatica2021/1c-Python/map-with-points.png', transparent=True, dpi=1000)
# plt.show()
'''

'''
margin = 10 # buffer to add to the range
lat_min = df.latitude.min() - margin
lat_max = df.latitude.max() + margin
lon_min = df.longitude.min() - margin
lon_max = df.longitude.max() + margin
# create map using BASEMAP
m = Basemap(llcrnrlon=lon_min,
            llcrnrlat=lat_min,
            urcrnrlon=lon_max,
            urcrnrlat=lat_max,
            lat_0=(lat_max - lat_min)/2,
            lon_0=(lon_max-lon_min)/2,
            projection='merc',
            resolution = 'h',
            area_thresh=10000.,
            )
m.drawcoastlines()
m.drawcountries()
m.drawstates()
#m.drawmapboundary(fill_color='#46bcec')
#m.fillcontinents(color = 'white',lake_color='#46bcec')
m.shadedrelief()
# convert lat and lon to map projection coordinates
lat = df['latitude'].values
lon = df['longitude'].values
lons, lats = m(lon, lat)
# plot points as red dots
m.scatter(lons, lats, marker = 'X', color='r', s=12)
plt.savefig('/home/fraverta/Dropbox/Informatica2021/1c-Python/map-with-points.png', transparent=True, dpi=1000)
# plt.show()

ENGINEERING_MAJORS = ['Ingeniería Aeronáutica', 'Ingeniería Ambiental', 'Ingeniería Biomédica', 'Ingeniería Civil',
                      'Ingeniería Electromecánica', 'Ingeniería Electrónica', 'Ingeniería Industrial',
                      'Ingeniería Mecánica', 'Ingeniería Química',
                      'Ingeniería en Agrimensura', 'Ingeniería en Computación']
ENGINEERING_MAJORS_CHOICES = [(f'{id}', major) for id, major in enumerate(ENGINEERING_MAJORS)]

c = Counter(df['major'].values)
data = [c[k] for k in sorted(c.keys())]
majors = [f'{c[k]}-{ENGINEERING_MAJORS[k]}' for k in sorted(c.keys())]
print(majors)
print(data)

fig, ax = plt.subplots(figsize=(18, 9), subplot_kw=dict(aspect="equal"))



wedges, texts = ax.pie(data, wedgeprops=dict(width=0.5), startangle=-40, textprops={'color':"w"})
bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
kw = dict(arrowprops=dict(arrowstyle="-"),
          bbox=bbox_props, zorder=0, va="center", fontsize=20)

for i, p in enumerate(wedges):
    ang = (p.theta2 - p.theta1)/2. + p.theta1
    y = np.sin(np.deg2rad(ang))
    x = np.cos(np.deg2rad(ang))
    horizontalalignment = {-1: "right", 1: "left"}[int(np.sign(x))]
    connectionstyle = "angle,angleA=0,angleB={}".format(ang)
    kw["arrowprops"].update({"connectionstyle": connectionstyle})
    ax.annotate(majors[i], xy=(x, y), xytext=(1.35*np.sign(x), 1.4*y),
                horizontalalignment=horizontalalignment, **kw)

#ax.set_title("")
plt.savefig('/home/fraverta/Dropbox/Informatica2021/1c-Python/pie_plot.png', transparent=True, dpi=1000)
plt.show()
'''