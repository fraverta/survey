# Generated by Django 3.1.7 on 2021-03-09 18:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cities_light', '0010_auto_20200508_1851'),
        ('singlepageapp', '0008_auto_20210309_1530'),
    ]

    operations = [
        migrations.AddField(
            model_name='surveyresponse',
            name='remoto',
            field=models.CharField(choices=[('1', 'Si'), ('0', 'No')], default='1', max_length=1, verbose_name='¿El lugar desde dónde cursará la materia coincide con su lugar de origen?'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='city',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='cities_light.city', verbose_name='Ciudad (Desde dónde cursará la materia)'),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='region',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='cities_light.region', verbose_name='Provincia/Region (Desde dónde cursará la materia)'),
        ),
    ]
