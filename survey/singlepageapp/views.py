from django.shortcuts import render
from .forms import SurveyResponseForm
from cities_light.models import Country, City, Region
from .plot import update_students_location_map, update_students_major_graph
# Create your views here.

def survey_entry_new(request):
    if request.method == "POST":
        form = SurveyResponseForm(request.POST)
        #form.cleaned_data['city'] = City.objects.get(pk=int(form.data['city']))
        if form.is_valid():
            survey_entry = form.save(commit=True)
            print("Survey Saved")
            return render(request, 'singlepageapp/survey_completed.html')
    else:
        form = SurveyResponseForm()
    return render(request, 'singlepageapp/survey_entry.html', {'form': form})


def load_region(request):
    country_id = request.GET.get('country')
    regions = Region.objects.filter(country_id=country_id).order_by('name')
    return render(request, 'singlepageapp/region_dropdown_list_options.html', {'regions': regions})

def load_cities(request):
    #country_id = request.GET.get('country')
    region_id = request.GET.get('region')
    cities = City.objects.filter(region_id=region_id).order_by('name')
    return render(request, 'singlepageapp/city_dropdown_list_options.html', {'cities': cities})

def display_map(request):
    #update_students_location_map()
    return render(request, 'singlepageapp/map_template.html')

def display_pie_plot(request):
    update_students_major_graph()
    return render(request, 'singlepageapp/pie_plot_template.html')