from cities_light.models import Country, City, Region
import pandas as pd


df = pd.read_csv('/home/fraverta/development/survey/survey/singlepageapp/localidades_db.csv')
cities = df.to_dict(orient='records')
country = Country.objects.filter(name='Argentina')[0]
'''
for city in cities:
    name = city['localidad_censal_nombre']
    lat =  float(city['centroide_lat'])
    long = float(city['centroide_lon'])
    qs = City.objects.filter(name=name)
    for saved_city in qs:
        if saved_city.country.name == 'Argentina' and  saved_city.region.country.name != 'Argentina':
            print(f"Country not Arg {saved_city.name} - {saved_city.region}")
            saved_city.delete()
'''

for city in cities:
    name = city['municipio_nombre']
    if type(name) != str:
        name = city['localidad_censal_nombre']
    lat =  city['centroide_lat']
    long = city['centroide_lon']
    provincia = city['provincia_nombre']

    if provincia == 'Córdoba':
        provincia = 'Cordoba'
    elif provincia == 'Río Negro':
        provincia = 'Rio Negro'
    elif provincia == 'Ciudad Autónoma de Buenos Aires':
        provincia = 'Buenos Aires F.D.'
    elif provincia == 'Neuquén':
        provincia = 'Neuquen'
    elif provincia == 'Tucumán':
        provincia = 'Tucuman'
    elif provincia == 'Entre Ríos':
        provincia = 'Entre Rios'
    elif provincia == 'Tierra del Fuego, Antártida e Islas del Atlántico Sur':
        provincia = 'Tierra del Fuego'
    #name = 'Menganito'
    #lat =  10
    #long = 20
    #provincia = 'Cordoba'
    if City.objects.filter(name=name, country_id=country).count() == 0:
        print(f'City {name} not present')
        try:
            provincia_obj = Region.objects.filter(name=provincia)[0]
            print(provincia_obj)
            assert provincia_obj.country.name == 'Argentina'
        except:
            print(f"{name} - {provincia} Can't be added")
            continue

        try:
            City.objects.create(name=name, latitude=float(lat), longitude=float(long), country=country, region=provincia_obj)
            print(f"{name} was successfully added")
        except:
            print(f"Create Error: {name} Can't be added")
